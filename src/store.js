import { createStore } from 'redux';

const initState = {
  elements: [],
  currSlide: -1,
  slideBar: { next: false, prev: false }
}

function counter(state, action) {
  if(typeof state === 'undefined') return initState;
  switch (action.type) {
    case 'SET_ELEMENT':
      return Object.assign({}, state, {elements: action.payload});
    case 'SET_CURRENT_SLIDE':
      return Object.assign({}, state, {currSlide: action.payload});
    case 'SET_SLIDEBAR':
      return Object.assign({}, state, {slideBar: action.payload});
    default:
      return state
  }
}

const store = createStore(counter);
store.subscribe(() => {
  console.log(store.getState())
})
export default store;
