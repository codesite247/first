export default [
  {id: 1, type: "Text", text: "content",
    actions: {
      next: 'BUTTON',
      prev: false
  }},
  {id: 1, type: "Image", duration: 5000, value: "https://blobscdn.gitbook.com/v0/b/gitbook-28427.appspot.com/o/spaces%2F-L5K1I1WsuQMZ8ecEuWg%2Favatar.png?generation=1518623866348435&alt=media",
    actions: {
      next: 'DURATION',
      prev: false
  }},
  {id: 1, type: "Text", text: "content 2",
    actions: {
      next: 'BUTTON',
      prev: false
  }},
  {id: 1, type: "Text", text: "some text",
    actions: {
      next: 'RIGHT_ARROW',
      prev: 'LEFT_ARROW'
  }}
]
