import React from 'react';
import _ from 'lodash';

const Text = ({ element }) => {

  return (
    <div
      style={
        _.has(element, 'position') ?
        {
          position: 'absolute',
          top: `${element.position[0]}%`,
          left:`${element.position[1]}%`,
          fontSize: element.fontSize,
          color: element.textColor
        } : {
          fontSize: element.fontSize,
          color: element.textColor
        }
      }
    >
      {element.text}
    </div>
  )
}

export default Text;
