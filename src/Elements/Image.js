import React from 'react';
import _ from 'lodash';

const Image = ({ element, currSlide, dispatch }) => {

  return (
    <img
      src={element.value}
      alt={element.value}
      className='slide-element'
      style={
        _.has(element, 'position') ?
        {
          position: 'absolute',
          top: `${element.position[0]}%`,
          left:`${element.position[1]}%`
        } : {}
      }
    />
  )
}

export default Image;
