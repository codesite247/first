import React from 'react';

const Video = ({ element }) => {

  return (
    <iframe width="560" height="315" src={element.value} frameBorder="0" allow="autoplay; encrypted-media" allowFullScreen></iframe>
  )
}

export default Video;
