import React from 'react';

const EmotivElement = ({children, currSlide, id, className}) => {
  return(
    <div style={{display: currSlide===id?'table-cell':'none'}} {...{className}}>{children}</div>
  )
}

export default EmotivElement;
