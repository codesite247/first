import React from 'react';
import { handleSlideBar } from '../actions';

const SlideBar = ({currSlide, slideBar, slideJump, className}) => {
  return (
    <div style={{display: slideBar.next==='BUTTON'?'block':'none'}} {...{className}}>
      <button onClick={() => slideJump(currSlide+1)}>Next</button>
    </div>
  )
}

export default SlideBar;
