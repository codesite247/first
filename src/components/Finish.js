import React from 'react';

const Finish = ({currSlide, className}) => (
  <div style={{display: currSlide==-2?'block':'none'}} {...{className}}>
    <button>Finish</button>
  </div>
);

export default Finish;
