import React from 'react';

const Prepair = ({currSlide, slideJump, className}) => (
  <div style={{display: currSlide==-1?'block':'none'}} {...{className}}>
    <button onClick={() => slideJump(0)}>Start</button>
  </div>
);

export default Prepair;
