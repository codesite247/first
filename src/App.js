import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from './store';
import Slider from './Slider';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Slider />
      </Provider>
    );
  }
}
export default App;
