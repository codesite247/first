import React from 'react';
import {connect} from 'react-redux';
import {
  handleElement,
  handleChangeSlide,
  handleSlideBar
} from './actions';

import scripts from './converter/converter';

import EmotivElement from './components/EmotivElement';
import SlideBar from './components/SlideBar';
import Prepair from './components/Prepair';
import Finish from './components/Finish';
import Text from './Elements/Text';
import Image from './Elements/Image';
import Video from './Elements/Video';
import Interval from './Elements/Interval';

import './App.css';

const ListElement = {
  Text, Image, Video, Interval
}

class Slider extends React.Component {
  componentWillMount = () => {
    const {dispatch} = this.props;
    dispatch(handleElement(scripts))
  }
  componentDidMount = () => {
    document.addEventListener("keydown", this.keyDownToNext, false);
  }
  keyDownToNext = (e) => {
    const {elements, currSlide} = this.props;
    const element = elements[currSlide];
    if(element.actions.next == 'RIGHT_ARROW' && e.keyCode == 39){ // RIGHT ARROW code: 39
      if(elements[currSlide]['phaseId'] == elements[currSlide+1]['phaseId']){
        this.nxtSlide();
      }
    }
    if(element.actions.prev == 'LEFT_ARROW' && e.keyCode == 37){ // LEFT ARROW code : 37
      if(elements[currSlide]['phaseId'] == elements[currSlide-1]['phaseId']){
        this.prvSlide();
      }
    }
  }
  nxtSlide = () => {
    const {elements, currSlide, dispatch} = this.props;
    if(currSlide < elements.length-1 ){
      this.slideJump(currSlide+1)
    }
    else{
      // go to finish
      dispatch(handleChangeSlide(-2));
    }
  }
  prvSlide = () => {
    const {elements, currSlide, dispatch} = this.props;
    if(currSlide > 0){
      var prv = currSlide-1;
      if(elements[currSlide-1]['type'].toLowerCase()=='interval'){
        prv = prv - 1;
      }
      this.slideJump(prv)
    }else{
      // do nothing
    }
  }
  elementRender = (element) => {
    const {currSlide, dispatch} = this.props;
    return React.createElement(
      ListElement[`${element.type}`],
      {
        element,
        currSlide, dispatch
      }
    )
  }
  slideJump = (number) => {
    const {elements, dispatch} = this.props;
    const element = elements[number];
    switch(element.actions.next){
      case 'DURATION':
          var duration = setInterval(() => {
            this.nxtSlide();
            clearInterval(duration);
          }, element.duration)
        break;
      case 'RIGHT_ARROW':
          console.log("aaa")

        break;
    }
    dispatch(handleSlideBar(element.actions));
    dispatch(handleChangeSlide(number));
  }
  render(){
    const {elements, currSlide, slideBar, dispatch} = this.props;
    return(
      <div className='slider'>
        <Prepair className='slide-prepair' {...{currSlide, slideJump: this.slideJump}} />
        {elements.map((item, key) => (
          <EmotivElement className='slide-item' {...{currSlide, key, id: key}}>{this.elementRender({...item, key})}</EmotivElement>
        ))}
        <SlideBar {...{elements, currSlide, slideBar, dispatch,
          slideJump: this.slideJump
        }} className='slide-bar' />
        <div className='slide-help' style={{display: currSlide==-1?'none':'block'}}>
          Next: {slideBar.next}
          <br />
          Prev: {slideBar.prev.toString()}
        </div>
        <Finish className='slide-finish' {...{currSlide}} />
      </div>
    )
  }
}

export default connect(state => {
  return {
    elements: state.elements,
    currSlide: state.currSlide,
    slideBar: state.slideBar,
    dispatch: state.dispatch
  }
})(Slider)
