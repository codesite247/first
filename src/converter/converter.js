import {phases, element, elements} from './data';

import { Random } from './configs';

// define intervals elements
const intervals = {...element, type: 'Interval'};

function getRandom(min, max){
  return Math.floor(Math.random() * max) + min
}
function converting(p, e){
  var data = new Array();
  p.map((item, k) => {
    var loop = 1;
    if(item.loop && item.loopSettings.number > 1){ // check if is looping phase, generate all element
      loop = item.loopSettings.number;
    }
    for(let i=1; i<=loop; i++){
      let new_e = e.filter(e => e.phaseId === item.id);
      let new_interval = [];
      let merge = [];
      new_e = Random(new_e, item);
      //new_e.map(o => ({...o, phaseType: item.subType}))
      new_e.map((each_e, key) => {
        new_e[key] = { ...new_e[key], phaseType: item.subType }; // map phase type to element

        if(item.fixation && item.fixation > 0){ // check interval settings
          new_interval[key] = { ...intervals, duration: item.fixation, value: item.durationSettings.image, phaseId: item.id }
          merge[key*2] = new_e[key];
          merge[key*2+1] = new_interval[key];
        }else{
          merge[key] = new_e[key];
        }
      });
      merge.map((m, j) => {
        // add CUSTOM Action
        if(!m.duration){
          switch(m['phaseType']){
            case 'calibration':
            case 'stimuli':
              merge[j] = { ...merge[j], actions: {
                next: 'BUTTON',
                prev: false
              }}
              break;
            case 'presentation':
              merge[j] = { ...merge[j], actions: {
                next: 'RIGHT_ARROW',
                prev: 'LEFT_ARROW'
              }}
              break;
          }
        }else{
          // add DURATION action
          merge[j] = { ...merge[j], actions: {
            next: 'DURATION',
            prev: false
          }}
        }
      });
      data = [...data, ...merge];
    }
  })
  return data;
}
const scripts = converting(phases, elements);

export default scripts;
