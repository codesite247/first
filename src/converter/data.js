export const phases = [
  {
    id: 1,
    name: 'phase name',
    order: 1,
    random: true,
    loop: false,
    fixation: 0,
    type: 'custom',
    subType: 'calibration',
    randomSettings: {
      randomType: 'random',
      random: {
        stimuliPosition: true,
        stimuliSize: false,
        intervals: false,
        stimuliColor: false,
        colors: ['black']
      },
      randomConstant: {
        stimuliPosition: false,
        stimuliSize: false,
        intervals: false,
        stimuliColor: false,
        colors: ['black']
      }
    },
    loopSettings: {
      number: 2
    },
    durationSettings: {
      image: null
    }
  },
  {
    id: 2,
    name: 'phase name',
    order: 1,
    random: true,
    loop: true,
    fixation: 4000,
    type: 'custom',
    subType: 'presentation',
    randomSettings: {
      randomType: 'random',
      random: {
        stimuliPosition: true,
        stimuliSize: true,
        intervals: true,
        stimuliColor: true,
        colors: ['orange', 'red', 'grey', 'blue']
      },
      randomConstant: {
        stimuliPosition: false,
        stimuliSize: false,
        intervals: false,
        stimuliColor: false,
        colors: ['black']
      }
    },
    loopSettings: {
      number: 2
    },
    durationSettings: {
      image: 'https://dantricdn.com/zoom/130_100/2018/7/3/ttck-giam-1530609315858529463552.jpg'
    }
  },
  {
    id: 3,
    name: 'phase name',
    order: 1,
    random: false,
    loop: false,
    fixation: 0,
    type: 'custom',
    subType: 'stimuli',
    randomSettings: {
      randomType: 'random',
      random: {
        stimuliPosition: false,
        stimuliSize: false,
        intervals: false,
        stimuliColor: false,
        colors: ['black']
      },
      randomConstant: {
        stimuliPosition: false,
        stimuliSize: false,
        intervals: false,
        stimuliColor: false,
        colors: ['black']
      }
    },
    loopSettings: {
      number: 2
    },
    durationSettings: {
      image: null
    }
  }
]
export const element = {
  id: 1,
  phaseId: 1,
  order: 1,
  type: 'Text',
  value: 'https://blobscdn.gitbook.com/v0/b/gitbook-28427.appspot.com/o/spaces%2F-L5K1I1WsuQMZ8ecEuWg%2Favatar.png?generation=1518623866348435&alt=media',
  questionType: null,
  duration: 5000,
  text: 'element text',
  disabled: false
}
export const elements = [
  { id: 1, phaseId: 1, order: 1, type: 'Image', value: 'https://dantricdn.com/zoom/327_245/2018/10/11/1-15391934018181272567281.jpg', questionType: null, duration: 5000, text: '', disabled: false},
  { id: 2, phaseId: 1, order: 1, type: 'Image', value: 'https://dantricdn.com/zoom/130_100/2018/10/10/opera-2-1539169968638482222504.jpg', questionType: null, duration: 3000, text: '', disabled: false},
  { id: 3, phaseId: 1, order: 1, type: 'Video', value: 'https://www.youtube.com/embed/jsQZaDbQ6Xk', questionType: null, duration: 0, text: '', disabled: false},
  { id: 4, phaseId: 2, order: 1, type: 'Text', value: null, questionType: null, duration: 0, text: 'text 2', disabled: false},
  { id: 5, phaseId: 2, order: 1, type: 'Image', value: 'https://dantricdn.com/zoom/130_100/2018/10/11/patriot-15392278819851015883997.jpg', questionType: null, duration: 0, text: '', disabled: false},
  { id: 6, phaseId: 3, order: 1, type: 'Text', value: null, questionType: null, duration: 0, text: 'text 3', disabled: false},
  { id: 7, phaseId: 3, order: 1, type: 'Image', value: 'https://dantricdn.com/zoom/130_100/2018/10/11/hlvmiura211-10-18-15392289460982144911432.jpg', questionType: null, duration: 5000, text: '', disabled: false},
  { id: 8, phaseId: 3, order: 1, type: 'Text', value: null, questionType: null, duration: 0, text: 'text 4', disabled: false},
  { id: 9, phaseId: 3, order: 1, type: 'Text', value: null, questionType: null, duration: 0, text: 'text 5', disabled: false},
  { id: 10, phaseId: 3, order: 1, type: 'Image', value: 'https://dantricdn.com/zoom/130_100/2018/10/11/anh-chup-man-hinh-2018-10-11-luc-09-crop-15392255885652060444593.png', questionType: null, duration: 2000, text: '', disabled: false}
]
