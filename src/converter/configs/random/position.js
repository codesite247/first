import { getRandom } from './random';

export const Position = (e) => {
  if(e.stimuliPosition) {
    let position = { x: [0, 80], y: [0, 80]}
    e.position = [getRandom(position.x[0], position.x[1]), getRandom(position.y[0], position.y[1])]
  }
}
