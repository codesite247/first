import { getRandom } from './random';

export const Size = (e) => {
  if(e.stimuliSize) {
    if(e.type.toLowerCase()==='text') { // check if Element type is TEXT, then randomize text color
      let size = {min: 6, max: 50}
      e.fontSize = getRandom(size.min, size.max)
    }
  }
}
