import { Position } from './position';
import { Size } from './size';
import { Color } from './color';

const Random = (elements, phase) => {
  elements.map((item, key) => {
    if(phase.random){ // check if is random phase, put random config to element attribute
      Object.assign(
        elements[key],
        {
          stimuliPosition: phase['randomSettings'][`${phase.randomSettings.randomType}`]['stimuliPosition'],
          stimuliSize: phase['randomSettings'][`${phase.randomSettings.randomType}`]['stimuliSize'],
          intervals: phase['randomSettings'][`${phase.randomSettings.randomType}`]['intervals'],
          stimuliColor: phase['randomSettings'][`${phase.randomSettings.randomType}`]['stimuliColor'],
          colors: phase['randomSettings'][`${phase.randomSettings.randomType}`]['colors'],
          position: function(){Position(this)},
          size: function(){Size(this)},
          color: function(){Color(this)}
        }
      );
      elements[key].position();
      elements[key].size();
      elements[key].color();
    }
  });
  return elements;
}

export default Random;
