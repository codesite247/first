import { getRandom } from './random';

export const Color = (e) => {
  if(e.stimuliColor) {
    if(e.type.toLowerCase()==='text') { // check if Element type is TEXT, then randomize text color
      e.textColor = e.colors[ getRandom(0, e.colors.length)]
    }
  }
}
