function handleElement(payload){
  return { type: "SET_ELEMENT", payload }
}
function handleChangeSlide(payload) {
  return { type: "SET_CURRENT_SLIDE", payload }
}
function handleSlideBar(payload) {
  return { type: "SET_SLIDEBAR", payload }
}
export {
  handleElement,
  handleChangeSlide,
  handleSlideBar
}
